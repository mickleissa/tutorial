terraform {
    required_providers {
        aws = {
            source = "hashicorp/aws"
            version = "2.7"
        }
    }
}

provider "aws" {
    profile = "Terraform"
    region = "us-east-1"
}

# resource "aws_instance" "workSpace" {
#   ami = "ami-0533f2ba8a1995cf9"
#   instance_type =lookup(var.instanceType,terraform.workspace)
#   }

# variable "instanceType" {
#     type = "map"

#     default ={
#         default = "t2.micro"
#         dev = "t2.nano"
#         prod = "t2.medium"
#         test = "t2.large"
#     }
# }